var express = require("express");
var knex = require("../config/knex");
var router = express.Router();
var jwt = require('jsonwebtoken');

router.get('/user/list', function(req, res){
    res.json({a: 1});
});

router.post('/user/login', function(req, res){
    var email = req.body.email;
    var password = req.body.password;

    var token = jwt.sign({email: email}, 'shhhh', function(err, token){
        var query = knex
                    .select('id', 'email')
                    .from('users')
                    .where({
                        email: email,
                        password: password
                    });
        query
        .then(function(rows){
            if(rows.length > 0){
                rows[0].token = token;
                res.json({data: rows[0], status: 200});
            }else{
                res.json({status: 401});
            }
        })
        .catch(function(error){
            res.status(500).json();
        })
    });
});

module.exports = router;