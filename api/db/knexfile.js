module.exports = {
    development: {
        client: 'sqlite3',
        connection: {
          filename: './swapez.db'
        },
        migrations: {
          tableName: "knexmig"
        }
    }
};