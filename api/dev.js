var express = require("express");
var bodyParser = require("body-parser");
var cors = require('cors');

var app = express();
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

var port = 7000;
var users = require("./routing/users");
app.use('/api', users);

app.listen(port, function(){
    console.log('listening on port '+port);
});