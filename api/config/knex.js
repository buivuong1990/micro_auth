var path = require("path");

var knex = require("knex")({
    client: 'sqlite3',
    connection: {
        filename: path.resolve('api', 'db', 'swapez.db')
    },
    debug: true,
    log: {
        warn(message){
            console.log('warning: ', message);
        },
        error(message){
            console.log('error: ', message);
        },
        deprecate(message) {
            console.log('deprecate: ',message);
        },
        debug(message) {
            console.log('debug: ',message);
        }
    }
});

module.exports = knex;