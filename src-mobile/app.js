import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import createHistory from "history/createBrowserHistory";
import { Provider } from "react-redux";
import configureStore from "./configureStore";

import Home from "./containers/home";
/*import Category from "./containers/category";
import Registration from "./containers/registration";
import AccountDevices from './containers/device';*/

import Menu from "./components/menu";
import QuickAccess from "./components/quickAccess";
import Footer from "./components/footer";

import Login from "./containers/login";

const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);

import withStorage from "./hoc/storage";

class App extends React.Component{
    constructor(props){
        super(props);
    }
    componentWillMount(){
        
    }
    render(){
        return (
            <Provider store={store}>
                <Router>
                    <div>
                        <div className="uk-height-viewport" uk-height-viewport="true">
                            <div className="uk-container-extend">
                                <Menu/>
                                <Route exact path="/" component={withStorage(Home)}/>
                                <Route exact path="/login" component={withStorage(Login)}/>
                                {/*<Route exact path="/login" component={Login}/>
                                <Route exact path="/registration" component={Registration}/>
                                <Route exact path="/category" component={Category}/>
                                <Route exact path="/account/devices" component={AccountDevices}/>*/}
                                <QuickAccess/>
                                <Footer/>
                            </div>
                        </div>
                    </div>
                </Router>
            </Provider>
        )
    }
}

export default App;