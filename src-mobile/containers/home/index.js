import React from "react";
import {connect} from "react-redux";

class Home extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div>
                <div className="slider">
                    <div className="uk-position-relative uk-visible-toggle uk-light" uk-slideshow="animation: scale;autoplay:true">
                        <ul className="uk-slideshow-items">
                            <li>
                                <img src="http://demo.swap-ez.com:1004/banner/mobile/1.jpg" alt="" uk-cover="true"/>
                            </li>
                            <li>
                                <img src="http://demo.swap-ez.com:1004/banner/mobile/2.jpg" alt="" uk-cover="true"/>
                            </li>
                            <li>
                                <img src="http://demo.swap-ez.com:1004/banner/mobile/3.jpg" alt="" uk-cover="true"/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        test: state.home.test
    }
}

export default Home;