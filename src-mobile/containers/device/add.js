import React from "react";
import axios from "axios";
import {connect} from "react-redux";

import 
    {
        GridInner, FormDefault, FormRow, 
        FormLegend, Divider, Button,
        FormLabel, FormGroup, InputText,
        Select2, InputNumber, InputRange,
        ImageUpload
    } 
from "../../components";

import withValidator from "../../hoc/validator";

class DeviceAdd extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            isImei: false,
            imei: '',
            
        }
        this.isValid = false;
    }
    componentDidMount(){
        
    }
    componentWillUnmount(){
        this.isValid = false;
    }
    handlerSubmit(){
        console.log('form', this.state);
        if(!this.state.isImei){
            if(this.state.imei === '123456')
                this.setState({isImei: !this.state.isImei});
        }
    }
    render(){
        return (
            <GridInner>
                <FormDefault onSubmit={this.handlerSubmit}>
                    <FormRow>
                        <FormLegend>PLEASE FILL IN THE FORM TO ADD YOUR DEVICES</FormLegend>
                    </FormRow>
                    <FormRow>
                        <Divider/>
                    </FormRow>
                    <FormRow width="1-2@s">
                        <FormGroup>
                            <FormLabel htmlFor="imei"
                                label="Imei | Serial Number"/>
                            <InputText placeholder="Type Imei | Serial Number" id="imei"
                                ref={instance => {
                                    this.props.component(instance, 'imei');
                                }}
                                onInput={value => this.state.imei = value}
                                value={this.state.imei}
                                validations={{
                                    required: true,
                                    maxLength: 50,
                                    minLength: 6
                                }}
                                disabled={this.state.isImei}
                                icon="comment"/>
                        </FormGroup>
                    </FormRow>
                    <FormRow>
                        {
                            this.state.isImei 
                            ?
                                <Button color="secondary" className="uk-margin-right"
                                    onClick={() => this.setState({imei: '', isImei: false})}>Change Imei</Button>
                            : null
                        }
                        <Button color="primary" type="submit" 
                            ref={instance => {
                                this.props.canSubmit(instance);
                            }}>Accept</Button>
                    </FormRow>
                    {/*<FormRow width="1-2@s">
                        <FormGroup>
                            <FormLabel htmlFor="category"
                                label="Category"/>
                            <InputText placeholder="Choose Category" id="category"
                                readOnly/>
                        </FormGroup>
                    </FormRow>
                    <FormRow width="1-2@s">
                        <FormGroup>
                            <FormLabel htmlFor="brand"
                                label="Brand"/>
                            <InputText placeholder="Choose Brand" id="brand"
                                readOnly/>
                        </FormGroup>
                    </FormRow>
                    <FormRow width="1-2@s">
                        <FormGroup>
                            <FormLabel htmlFor="model" 
                                label="Model"/>
                            <InputText placeholder="Choose Model" id="model"
                                readOnly/>
                        </FormGroup>
                    </FormRow>
                    <FormRow>
                        <Divider/>
                    </FormRow>
                    <FormRow width="1-2@s">
                        <FormGroup>
                            <FormLabel htmlFor="color"
                                label="Color"/>
                            <Select2 id="color"
                                placeholder="Choose Color"
                                list={
                                    [
                                        {id: 1, name: 'Black'},
                                        {id: 2, name: 'Yellow'},
                                        {id: 3, name: 'Red'}
                                    ]
                                }/>
                        </FormGroup>
                    </FormRow>
                    <FormRow width="1-2@s">
                        <FormGroup>
                            <FormLabel htmlFor="capacity" 
                                label="Capacity"/>
                            <Select2 id="capacity"
                                placeholder="Choose Capacity"
                                list={
                                    [
                                        {id: 1, name: '4GB'},
                                        {id: 2, name: '8GB'},
                                        {id: 3, name: '16GB'}
                                    ]
                                }/>
                        </FormGroup>
                    </FormRow>
                    <FormRow width="1-2@s">
                        <FormGroup>
                            <FormLabel htmlFor="imei" 
                                label="Price"/>
                            <InputNumber placeholder="Type Price" id="price"/>
                        </FormGroup>
                    </FormRow>
                    <FormRow width="1-2@s">
                        <FormGroup>
                            <FormLabel htmlFor="condition" 
                                label="Condition Of Your Device"/>
                            <InputRange id="condition"/>
                        </FormGroup>
                    </FormRow>
                    <FormRow>
                        <Divider/>
                    </FormRow>
                    <FormRow>
                        <FormLabel htmlFor="images"
                            label="Choose your device images"/>
                        <div className="uk-margin-top">
                            <ImageUpload maxFiles={5}/>
                        </div>
                    </FormRow>
                    <FormRow>
                        <Divider/>
                    </FormRow>
                    */}
                </FormDefault>
            </GridInner>
        )
    }
}

function mapStateToProps(state){
    return {
        test: state.home.test
    }
}

export default withValidator(DeviceAdd);