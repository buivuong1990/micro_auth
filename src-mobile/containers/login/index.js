import React from "react";
import axios from "axios";
import 
    {
        FormDefault, FormRow, 
        Button, FormGroup, InputText,
        InputPassword,
        Alert,
        Cover,
        Spinner
    } 
from "../../components";
import { SERVER_API } from "../../config";
import {Redirect} from "react-router";

class Login extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            email: '',
            password: '',
            isError: false,
            message: '',
            isLoading: false,
            isRedirect: false
        }
    }
    componentDidMount(){
        
    }
    handlerSubmit(){
        if(is.empty(this.state.email) && is.empty(this.state.password)){
            this.setState({isError: true, message: 'Email & Password Not Empty'});
            return;
        }
        this.handlerApiLogin();
    }
    handlerApiLogin(){
        const {email, password} = this.state;
        this.setState({isLoading: true});
        axios.post(SERVER_API+'user/login', {email, password})
        .then(response => {
            this.setState({isLoading: false}, () => {
                const data = response.data;
                const {status} = data;
                if(Number(status) === 401)
                    this.setState({isError: true, message: 'Email Or Password Failed'});
                else
                    this.handlerGlobalStorage();
            });
        })
        .catch(error => {
            this.setState({isLoading: false, isError: true, message: 'Our Website are in construction'});
        })
    }
    handlerGlobalStorage(){
        UIkit.notification({message: 'Login Successfully !', status: 'primary', pos: 'top-left'});
        localStorage.setItem('email', this.state.email);
        setTimeout(() => {
            this.setState({isRedirect: true});
        }, 500);
    }
    render(){
        return (
            <div>
                {this.state.isRedirect ? <Redirect to="/"/> : null}
                <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Login</div>
                </div>
                <div className="uk-padding-small">
                    <div className="uk-flex uk-flex-center uk-margin-top uk-margin-bottom">
                        <i className="fa fa-user uk-icon-login"></i>
                    </div>
                    <div className="uk-inline">
                        {
                            this.state.isLoading ? <Cover/> : null
                        }
                        {
                            this.state.isLoading ? 
                            <Spinner/> : null
                        }
                        <FormDefault onSubmit={this.handlerSubmit}>
                            {
                                this.state.isError
                                ? 
                                <FormRow>
                                    <FormGroup>
                                        <Alert message={this.state.message}
                                            onBeforeHide={() => this.setState({isError: false})}/>
                                    </FormGroup>
                                </FormRow>
                                : null
                            }                               
                            <FormRow>
                                <FormGroup>
                                    <InputText placeholder="Your Email" id="email"
                                        onInput={value => this.state.email = value}
                                        value={this.state.email}
                                        icon="mail"/>
                                </FormGroup>
                            </FormRow>
                            <FormRow>
                                <FormGroup>
                                    <InputPassword placeholder="Your Password" id="password"
                                        onInput={value => this.state.password = value}
                                        value={this.state.password}
                                        icon="lock"/>
                                </FormGroup>
                            </FormRow>
                            <FormRow>
                                <FormGroup>
                                    <Button className="uk-width-1-1 uk-margin-xsmall-top" type="submit" color="primary">
                                        Login
                                    </Button>
                                </FormGroup>
                            </FormRow>
                        </FormDefault>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;