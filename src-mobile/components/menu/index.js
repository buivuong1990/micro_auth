import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../hoc/storage";

class Menu extends React.Component{
    constructor(props){
        super(props);
        this.navigateToRegistration = this.navigateToRegistration.bind(this);
        this.navigateToLogin = this.navigateToLogin.bind(this);
        this.navigateToHome = this.navigateToHome.bind(this);
        this.handlerLogout = this.handlerLogout.bind(this);
    }
    navigateToHome(){
        this.props.history.push('/');
        setTimeout(() => {
            UIkit.offcanvas($('#offcanvas-slide')).hide();
        }, 100);
    }
    navigateToLogin(){
        this.props.history.push('/login');
        setTimeout(() => {
            UIkit.offcanvas($('#offcanvas-slide')).hide();
        }, 100);
    }
    navigateToRegistration(){
        this.props.history.push('/registration');
    }
    handlerLogout(){
        localStorage.removeItem('email');
        UIkit.offcanvas($('#offcanvas-slide')).hide();
        setTimeout(() => {
            this.props.history.push(this.props.history.location.pathname);
            UIkit.notification({message: 'Logout Successfully !', status: 'primary', pos: 'top-left'});
        }, 500);
    }
    render(){
        return (
            <nav uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky;" className="uk-height-small uk-navbar-container" uk-navbar="true">
                <div className="uk-navbar-left">
                    <a uk-toggle="target: #offcanvas-slide" className="uk-navbar-toggle" uk-navbar-toggle-icon=""></a>
                    <div id="offcanvas-slide" uk-offcanvas="true">
                        <div className="uk-offcanvas-bar uk-padding-remove">
                            <div className="uk-flex uk-flex-column">
                                <div className="uk-height-small"/>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-text-bold uk-link-reset uk-padding"
                                    onClick={this.navigateToHome}>
                                    <i className="uk-margin-right fa fa-home" aria-hidden="true"></i>
                                    <span>Home</span>
                                </a>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-text-bold uk-link-reset uk-padding">
                                    <i className="uk-margin-right fa fa-mobile-phone" aria-hidden="true"></i>
                                    <span>Smart Phones</span>
                                </a>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-text-bold uk-link-reset uk-padding">
                                    <i className="uk-margin-right fa fa-tablet" aria-hidden="true"></i>
                                    <span>Tablets</span>
                                </a>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-text-bold uk-link-reset uk-padding">
                                    <i className="uk-margin-right fa fa-clock-o" aria-hidden="true"></i>
                                    <span>Smart Watches</span>
                                </a>
                                <a className="uk-height-small uk-flex uk-flex-middle uk-text-bold uk-link-reset uk-padding">
                                    <i className="uk-margin-right fa fa-question-circle-o" aria-hidden="true"></i>
                                    <span>Support</span>
                                </a>
                                {
                                    !this.props.auth
                                    ? <a className="uk-height-small uk-flex uk-flex-middle uk-text-bold uk-link-reset uk-padding">
                                        <i className="uk-margin-right fa fa-user-plus" aria-hidden="true"></i>
                                        <span>Registration</span>
                                    </a>
                                    : null
                                }
                                {
                                    !this.props.auth
                                    ? <a className="uk-height-small uk-flex uk-flex-middle uk-text-bold uk-link-reset uk-padding"
                                        onClick={this.navigateToLogin}>
                                        <i className="uk-margin-right fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Login</span>
                                    </a>
                                    : null
                                }
                                {
                                    this.props.auth
                                    ? <a className="uk-height-small uk-flex uk-flex-middle uk-text-bold uk-link-reset uk-padding"
                                        onClick={this.handlerLogout}>
                                        <i className="uk-margin-right fa fa-sign-out" aria-hidden="true"></i>
                                        <span>Logout</span>
                                    </a>
                                    : null
                                }
                            </div>
                            {/*<ul className="uk-nav uk-nav-default uk-nav-parent-icon" uk-nav="true">
                                <li className="uk-height-small"></li>
                                <li className="uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-bottom uk-padding-remove-top">
                                    <a className="uk-width-1-1" onClick={this.navigateToHome}><i className="uk-margin-right fa fa-home" aria-hidden="true"></i> Home</a>
                                </li>
                                <li className="uk-height-small uk-flex uk-flex-column uk-flex-center uk-padding-small uk-padding-remove-bottom uk-padding-remove-top uk-parent">
                                    <a><i className="uk-margin-right fa fa-check-circle" aria-hidden="true"></i> Your purpose</a>
                                    <ul className="uk-nav-sub">
                                        <li className="uk-height-small uk-flex uk-flex-column uk-flex-center">
                                            <a href="#"><i className="uk-margin-right fa fa-tag" aria-hidden="true"></i> Sale</a>
                                        </li>
                                        <li className="uk-height-small uk-flex uk-flex-column uk-flex-center">
                                            <a href="#"><i className="uk-margin-right fa fa-shopping-cart" aria-hidden="true"></i> Buy</a>
                                        </li>
                                        <li className="uk-height-small uk-flex uk-flex-column uk-flex-center">
                                            <a href="#"><i className="uk-margin-right fa fa-exchange" aria-hidden="true"></i> Exchange</a>
                                        </li>
                                    </ul>
                                </li>
                                <li className="uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-bottom uk-padding-remove-top">
                                    <a className="uk-width-1-1"><i className="uk-margin-right fa fa-mobile-phone" aria-hidden="true"></i> Smart phone</a>
                                </li>
                                <li className="uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-bottom uk-padding-remove-top">
                                    <a href="#"><i className="uk-margin-right fa fa-tablet" aria-hidden="true"></i> Tablet</a>
                                </li>
                                <li className="uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-bottom uk-padding-remove-top">
                                    <a href="#"><i className="uk-margin-right fa fa-clock-o" aria-hidden="true"></i> Smart watch</a>
                                </li>
                                <li className="uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-bottom uk-padding-remove-top">
                                    <a href="#"><i className="uk-margin-right fa fa-question-circle-o" aria-hidden="true"></i> Support</a>
                                </li>
                                <li className="uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-bottom uk-padding-remove-top">
                                    <a href="#"><i className="uk-margin-right fa fa-user-plus" aria-hidden="true"></i> Registration</a>
                                </li>
                                <li>
                                    <a className="uk-width-1-1 uk-height-small uk-flex uk-flex-middle uk-flex-center">
                                        <i className="uk-margin-right fa fa-sign-out" aria-hidden="true"/>
                                        <span>Login</span>
                                    </a>
                                </li>
        </ul>*/}
                        </div>
                    </div>
                </div>
                <div className="uk-navbar-center">
                    <a onClick={this.navigateToHome} className="uk-navbar-item uk-logo"><i className="uk-text-white fa fa-refresh" aria-hidden="true"></i></a>
                </div>
                <div className="uk-navbar-right">
                    <a href="" className="uk-navbar-item uk-logo">
                        <i className="uk-text-white uk-inline fa fa-shopping-bag no-ripple" aria-hidden="true">
                        <span className="uk-badge uk-position-top-right">1</span>
                        </i>
                    </a>
                </div>
            </nav>
        )
    }
}

export default withRouter(withStorage(Menu));