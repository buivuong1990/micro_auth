import React from "react";

class Footer extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <footer className="uk-padding-small uk-margin-top">
                <div>
                    <p className="uk-text-bold uk-text-muted uk-text-small"><span className="uk-text-emphasis">Contact</span> Swap-ez customer service professionals by phone or email.</p>
                </div>
                <hr className="uk-divider-icon"/>
                <div>
                    <img src="http://demo.swap-ez.com/templates/swap-ez/img/icon/english.jpg" width="20px"/>
                    <span className="uk-text-meta uk-text-primary uk-margin-xsmall-left">English</span>
                </div>
                <div>
                    <p className="uk-text-bold uk-text-muted uk-text-small">Copyright © 2017 Swap-ez Inc. All rights reserved.</p>
                </div>
                <div className="uk-flex uk-flex-wrap">
                    <div>
                        <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" href="">Privacy</a>
                    </div>
                    <div><a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" href="">Terms of Us</a></div>
                    <div><a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" href="">Sales and Retunds</a></div>
                    <div><a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" href="">Legal</a></div>
                    <div><a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-small-right" href="">Site Map </a></div>
                </div>
            </footer>
        )
    }
}

export default Footer;