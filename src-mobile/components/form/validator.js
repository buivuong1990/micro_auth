const Validate = {
    validate: (instance, value, isStatic = false) => {
        let flag = true;
        const validations = instance.props.validations;
        if(is.existy(validations["email"]) && value.length > 0 && flag){
            if(is.not.email(value)){
                const message = v.titleCase(instance.name) + ' format wrong !';
                Validate.customValidatorError(instance, value, message, isStatic);
                flag = false;
            }
        }
        if(is.existy(validations["maxLength"]) && value.length > 0 && flag){
            if(value.length > validations["maxLength"]){
                const message = v.titleCase(instance.name) + ' have max character of '+validations["maxLength"]+' !';
                Validate.customValidatorError(instance, value, message, isStatic);
                flag = false;
            }
        }
        if(is.existy(validations["maxLength"]) && value.length > 0 && flag){
            if(value.length < validations["minLength"]){
                const message = v.titleCase(instance.name) + ' have min character of '+validations["minLength"]+' !';
                Validate.customValidatorError(instance, value, message, isStatic);
                flag = false;
            }
        }
        if(is.existy(validations["compareEqual"]) && value.length > 0 && flag){
            const otherField = instance.validator.field[validations["compareEqual"]];
            if(otherField.state.value !== value){
                const message = v.titleCase(instance.name) + ' must equal with field '+validations["compareEqual"]+' !';
                Validate.customValidatorError(instance, value, message, isStatic);
                flag = false;
            }
        }
        if(is.existy(validations["required"]) && flag){
            if(is.empty(value)){
                const message = v.titleCase(instance.name) + ' required !';
                Validate.customValidatorError(instance, value, message, isStatic);
                flag = false;
            }
        }
        if(flag){
            if(is.existy(validations["compareEqual"])){
                const otherField = instance.validator.field[validations["compareEqual"]];
                if(v.indexOf(otherField.state.message, 'must equal with field') > -1)
                    Validate.customValidatorSuccess(otherField, otherField.state.value, false);
            }
            Validate.customValidatorSuccess(instance, value, isStatic);
        }
    },
    customValidatorError: (instance, value, message, isStatic = false) => {
        instance.isValid = false;
        if(isStatic){
            instance.validator.triggerChange();
        }else{
            instance.setState({error: 1, message: message, value: value}, () => {
                instance.validator.triggerChange();
                if(is.function(instance.props.onInput))
                    instance.props.onInput('');
                instance.isTouch = true;
            });
        }
    },
    customValidatorSuccess: (instance, value, isStatic = false) => {
        instance.isValid = true;
        if(isStatic)
            instance.validator.triggerChange();
        else{
            const error = instance.isTouch ? 2 : 0;
            instance.setState({error: error, message: '', value: value}, () => {
                instance.validator.triggerChange();
                if(is.function(instance.props.onInput))
                    instance.props.onInput(value);
            });
        }
    }
}

export default Validate;