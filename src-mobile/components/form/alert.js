import React from "react";

export class Alert extends React.Component{
    constructor(props){
        super(props);
        this.instanceRef = null;
    }
    componentDidMount(){
        UIkit.alert(this.instanceRef);
        $(this.instanceRef).on('beforehide', () => {
            this.props.onBeforeHide();
        });
    }
    componentWillUnmount(){
        this.instanceRef = null;
    }
    render(){
        return (
            <div className="uk-background-danger uk-light uk-width-1-1 uk-margin-remove
                uk-flex uk-flex-between uk-padding-remove-right"
                ref={instance => this.instanceRef = instance}>
                <div>{this.props.message}</div>
                <a className="uk-margin-xsmall-right" uk-icon="icon: close"
                    onClick={() => UIkit.alert(this.instanceRef).close()}></a>
            </div>
        )
    }
}