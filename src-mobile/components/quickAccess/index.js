import React from "react";

class QuickAccess extends React.Component{
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Quick Access</div>
                </div>
                <div className="uk-child-width-1-2 uk-grid uk-grid-small uk-text-center uk-padding-small" uk-grid="">
                    <div className="uk-card">
                                <div className="uk-quick-access-item uk-text-muted uk-text-small">
                            <div><span uk-icon="icon:phone;ratio:3.5"></span></div>
                            <span>Product</span>
                        </div>
                    </div>
                        <div className="uk-card">
                        <div className="uk-quick-access-item uk-text-muted uk-text-small">
                            <div><span uk-icon="icon:user;ratio:3.5"></span></div>
                            <span>My Account</span>
                        </div>
                    </div>
                    <div className="uk-card">
                        <div className="uk-quick-access-item uk-text-muted uk-text-small">
                            <div><span uk-icon="icon:users;ratio:3.5"></span></div>
                            <span>About Swap-ez</span>
                        </div>
                    </div>
                    <div className="uk-card">
                        <div className="uk-quick-access-item uk-text-muted uk-text-small">
                            <div><span uk-icon="icon:info;ratio:3.5"></span></div>
                            <span>How it work</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
 export default QuickAccess;