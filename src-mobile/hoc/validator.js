import React from "react";

const withValidator = (WrappedComponent) => {
    class HOC extends React.Component{
        constructor(props){
            super(props);
            this.field = {};
            this.wrappedComponentRef = null;
            this.canSubmit = null;
        }
        componentDidMount(){
            console.log('sasaasas', WrappedComponent);
        }
        componentWillUnmount(){
            this.field = null;
            this.wrappedComponentRef = null;
        }
        shouldComponentUpdate(){
            return false;
        }
        triggerChange(){
            let isValid = true;
            for(var key in this.field){
                if(this.field.hasOwnProperty(key)){
                    const field = this.field[key];
                    isValid = isValid & field.isValid;
                }
            }
            isValid = isValid ? true: false;
            this.isValid = isValid;
            this.wrappedComponentRef.isValid = isValid;
            if(is.not.null(this.canSubmit))
                this.canSubmit.toggleDisabled(isValid);
        }
        component(instance, name){
            if(is.existy(instance)){
                if(is.not.existy(this.field[name])){
                    instance.setValidator(this, name);
                    this.field[name] = instance;
                }
            }
        }
        canSubmitFunc(instance){
            if(is.null(this.canSubmit)){
                this.canSubmit = instance;
            }
        }
        render(){
            const props = {
                component: this.component.bind(this),
                canSubmit: this.canSubmitFunc.bind(this)
            }
            return <WrappedComponent {...props} ref={instance => this.wrappedComponentRef = instance}/>;
        }
    }
    return HOC;
}

export default withValidator;