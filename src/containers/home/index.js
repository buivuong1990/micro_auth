import React from "react";
import {connect} from "react-redux";

class Home extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div>
                <div id="modal-center" className="uk-modal-full" uk-modal="true" uk-overflow-auto="true">
                    <div className="uk-modal-dialog">
                        <button className="uk-modal-close-full uk-close-large" type="button" uk-close="true"></button>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <button onClick={() => alert('Samaria hariasa')}>Now a day</button>
                        <div className="uk-height-viewport">
                            ssasasasa
                        </div>
                    </div>
                </div>
                <div className="uk-position-relative uk-visible-toggle uk-light" uk-slideshow="animation: scale; autoplay: true; ratio: 7:3">
				    <ul className="uk-slideshow-items">
				        <li>
                            <div className="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center-left">
				                <img src="http://demo.swap-ez.com:1004/banner/mobile/1.jpg" alt="" uk-cover="true"/>
                            </div>
				        </li>
				        <li>
                            <div className="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center-left">
				                <img src="http://demo.swap-ez.com:1004/banner/mobile/2.jpg" alt="" uk-cover="true"/>
                            </div>
				        </li>
				        <li>
                            <div className="uk-position-cover uk-animation-kenburns uk-animation-reverse uk-transform-origin-center-left">
				          	    <img src="http://demo.swap-ez.com:1004/banner/mobile/3.jpg" alt="" uk-cover="true"/>
                            </div>
				        </li>
				    </ul>
				</div>
                
                <div className="uk-container uk-margin-large-top uk-margin-large-bottom">
                    <div className="uk-grid uk-grid-small" uk-grid="true">
                        <div className="uk-width-1-3@l uk-cursor">
                            <div className="uk-animation-toggle">
                                <div className="uk-background-top-right uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-middle uk-flex-center
                                    uk-animation-fade"
                                    style={{backgroundImage: `url(http://demo.swap-ez.com/assets/purpose-3.f35f06dd94ea2f3bd8e77baeef2889b7.jpg)`}}>
                                    <p className="uk-h3 uk-text-white uk-text-uppercase">Sale</p>
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-3@l uk-cursor">
                            <div className="uk-animation-toggle">
                                <div className="uk-background-top-right uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-middle uk-flex-center
                                    uk-animation-fade"
                                    style={{backgroundImage: `url(http://demo.swap-ez.com/assets/purpose-2.7fe9a8bb8df5bbbd5ac444f2191e8c7b.jpg)`}}>
                                    <p className="uk-h3 uk-text-white uk-text-uppercase">Buy</p>
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-3@l uk-cursor">
                            <div className="uk-animation-toggle">
                                <div className="uk-background-top-right uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-middle uk-flex-center
                                    uk-animation-fade"
                                    style={{backgroundImage: `url(http://demo.swap-ez.com/assets/purpose-1.0b3f74d82fe03f71b8a5d7c43687b3c1.jpg)`}}>
                                    <p className="uk-h3 uk-text-white uk-text-uppercase">Exchange</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="uk-container uk-margin-large-top uk-margin-large-bottom">
                    <article className="uk-article uk-margin-bottom">
                        <a className="uk-link-reset uk-text-uppercase uk-h4">Featured product</a>
                    </article>
                    <div className="uk-position-relative uk-visible-toggle" uk-slider="true">
                        <ul className="uk-slider-items uk-child-width-1-1@s uk-child-width-1-2@m uk-child-width-1-3@l uk-child-width-1-4@xl uk-grid uk-grid-small uk-grid-match">
                            <li className="uk-transition-toggle">
                                <div className="uk-card uk-card-hover uk-card-small uk-card-body uk-inline">
                                    <div className="uk-position-top-right uk-overlay uk-overlay-default uk-transition-slide-bottom-small">
                                        Top Right
                                    </div>
                                    <div className="uk-flex-center">
                                        <img src="http://demo.swap-ez.com:1004/products/images-1539294892080.jpg"/>
                                    </div>
                                    <h4 className="uk-text-truncate">BPhone 3 đậm khí chất</h4>
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-flex-between">
                                                <div className="uk-text-bold">Sale</div>
                                                <div>$ 1,500</div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div className="uk-card-footer uk-padding-remove uk-transition-slide-bottom-small">
                                        <div className="uk-grid uk-grid-collapse" uk-grid="true">
                                            <div className="uk-width-1-2">
                                                <button className="uk-button uk-button-primary uk-width-1-1 uk-padding-remove">Buy</button>
                                            </div>
                                            <div className="uk-width-1-2">
                                                <button className="uk-button uk-button-secondary uk-width-1-1 uk-padding-remove"
                                                    onClick={() => UIkit.modal('#modal-center').show()}>Exchange</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li className="uk-transition-toggle">
                                <div className="uk-card uk-card-hover uk-card-small uk-card-body">
                                    <img src="http://demo.swap-ez.com:1004/products/images-1539250151431.jpeg"/>
                                    <h4 className="uk-text-truncate">BPhone 4 sắp ra</h4>
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-flex-between">
                                                <div className="uk-text-bold">Exchange</div>
                                                <div>$ 1,500</div>
                                            </div>
                                            <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                                                <i className="fa fa-exchange"/>
                                                <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange With: Galaxy Note 4</div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div className="uk-card-footer uk-padding-remove" className="uk-transition-slide-bottom-small">
                                        <div className="uk-grid uk-grid-collapse" uk-grid="true">
                                            <div className="uk-width-1-2">
                                                <button className="uk-button uk-button-primary uk-width-1-1 uk-padding-remove">Buy</button>
                                            </div>
                                            <div className="uk-width-1-2">
                                                <button className="uk-button uk-button-secondary uk-width-1-1 uk-padding-remove">Exchange</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="uk-card uk-card-hover uk-card-body">
                                    <img src="http://demo.swap-ez.com:1004/products/images-1539250319171.jpg"/>
                                    <h4 className="uk-text-truncate">BPhone 5 lừa dối khán giả</h4>
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-flex-between">
                                                <div className="uk-text-bold">Sale</div>
                                                <div>$ 1,500</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-flex-between">
                                                <div className="uk-text-bold">Exchange</div>
                                                <div>$ 1,500</div>
                                            </div>
                                            <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                                                <i className="fa fa-exchange"/>
                                                <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange With: Galaxy Note 4</div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        test: state.home.test
    }
}

export default Home;