import React from "react";
import axios from "axios";
import 
    {
        FormDefault, FormRow, 
        FormLegend, Button, FormGroup, InputText,
        InputPassword,
        Alert,
        GridCenter,
        Card,
        Cover,
        Spinner
    } 
from "../../components";
import { SERVER_API } from "../../config";
import {Redirect} from "react-router";

class Account extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isRedirect: false
        }
    }
    componentDidMount(){
        
    }
    render(){
        return (
            <div>
                {this.state.isRedirect ? <Redirect to="/"/> : null}
                My Account
            </div>
        )
    }
}

export default Account;