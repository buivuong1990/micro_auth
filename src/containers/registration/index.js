import React from "react";
import {connect} from "react-redux";
import Recaptcha from "react-recaptcha";
import axios from "axios";

import withValidator from "../../hoc/validator";

import 
    {
        GridHorizontal, FormDefault, FormRow, 
        FormLegend, Divider, Button,
        FormGroup, InputText, InputPassword,
        Checkbox
    } 
from "../../components";

class Registration extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            email: '',
            password: '',
            confirmPassword: ''
        }
    }
    componentWillUnmount(){
        this.isValid = true;
    }
    handlerSubmit(){
        console.log('state', this.state, this.isValid);
    }
    render(){
        return (
            <GridHorizontal>
                <FormDefault onSubmit={this.handlerSubmit}>
                    <FormRow>
                        <FormLegend>REGISTER SWAP-EZ</FormLegend>
                    </FormRow>
                    <FormRow>
                        <Divider/>
                    </FormRow>
                    <FormRow>
                        <FormGroup>
                            <InputText placeholder="Your Email" id="email"
                                ref={instance => {
                                    this.props.component(instance, 'email');
                                }}
                                onInput={value => this.state.email = value}
                                value={this.state.email}
                                type="email"
                                validations={{
                                  required: true,
                                  email: true,
                                  maxLength: 50,
                                  minLength: 6
                                }}
                                icon="mail"/>
                        </FormGroup>
                    </FormRow>
                    <FormRow>
                        <FormGroup>
                            <InputPassword placeholder="Your Password" id="password"
                                ref={instance => {
                                    this.props.component(instance, 'password');
                                }}
                                onInput={value => this.state.password = value}
                                value={this.state.password}
                                validations={{
                                    required: true,
                                    maxLength: 50,
                                    minLength: 6,
                                    compareEqual: 'confirmPassword'
                                }}
                                icon="lock"/>
                        </FormGroup>
                    </FormRow>
                    <FormRow>
                        <FormGroup>
                            <InputPassword placeholder="Password Confirm" id="passwordConfirm"
                                ref={instance => {
                                    this.props.component(instance, 'confirmPassword');
                                }}
                                onInput={value => this.state.confirmPassword = value}
                                value={this.state.confirmPassword}
                                validations={{
                                    required: true,
                                    maxLength: 50,
                                    compareEqual: 'password'
                                }}
                                icon="lock"/>
                        </FormGroup>
                    </FormRow>
                    {/*<FormRow>
                        <FormGroup>
                            <Recaptcha
                                sitekey="6Le1m3cUAAAAALV7RMhLQ35tgU_UlRm8XrIKReis"
                                render="explicit"
                                verifyCallback={this.handleVerifyCallback}
                            />
                        </FormGroup>
                    </FormRow>*/}
                    <FormRow>
                        <FormGroup>
                            <Checkbox label={"Sign Up For Our Newsletters !"}/>
                        </FormGroup>
                    </FormRow>
                    <FormRow>
                        <FormGroup>
                            <Checkbox label={"Receive Special Offers From Our Partners !"}/>
                        </FormGroup>
                    </FormRow>
                    <FormRow>
                        <Divider/>
                    </FormRow>
                    <FormRow>
                        <Button className="uk-width-1-1" color="primary" type="submit">
                            Register
                        </Button>
                    </FormRow>
                </FormDefault>
                <div>test 2</div>
            </GridHorizontal>
        )
    }
}

function mapStateToProps(state){
    return {
        test: state.home.test
    }
}

export default withValidator(Registration);