import React from "react";

class Footer extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="uk-background-primary">
                <div className="uk-container uk-padding-top uk-padding">
                    <div className="uk-grid uk-grid-medium" uk-grid="true">
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-4@l">
                            <div className="uk-flex uk-flex-middle uk-height-small">
                                <img data-src="http://demo.swap-ez.com/assets/logo2.bfe8cc381ae464b6045494b74be163fd.png"
                                    width="50" height="50" uk-img="true"
                                    className="uk-margin-xsmall-right"/>
                                <h5 className="uk-heading-line uk-margin-remove uk-text-white">SWAP*EZ</h5>
                            </div>
                            <ul className="uk-list uk-list-large">
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <i className="fa fa-phone uk-margin-xsmall-right"/>
                                    <span>
                                        (098)-686-666888
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <i className="fa fa-envelope uk-margin-xsmall-right"/>
                                    <span>
                                        email@swap*ez.com
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <i className="fa fa-map uk-margin-xsmall-right"/>
                                    <span>
                                        248 Avenue, Quebec, Canada
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-4@l">
                            <div className="uk-flex uk-flex-middle uk-height-small">
                                <span className="uk-heading-border uk-margin-xsmall-right"/>
                                <h5 className="uk-heading-line uk-light uk-margin-remove uk-text-white">PRODUCTS</h5>
                            </div>
                            <ul className="uk-list uk-list-large">
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Smart Phones
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Tablets
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Smart Watches
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Sell Products
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Exchange Products
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-4@l">
                            <div className="uk-flex uk-flex-middle uk-height-small">
                                <span className="uk-heading-border uk-margin-xsmall-right"/>
                                <h5 className="uk-heading-line uk-light uk-margin-remove uk-text-white">MY ACCOUNT</h5>
                            </div>
                            <ul className="uk-list uk-list-large">
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Registration
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        My Cart
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Checkout
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        My Account
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        My Wishlist
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div className="uk-width-1-1@s uk-width-1-2@m uk-width-1-4@l">
                            <div className="uk-flex uk-flex-middle uk-height-small">
                                <span className="uk-heading-border uk-margin-xsmall-right"/>
                                <h5 className="uk-heading-line uk-light uk-margin-remove uk-text-white">ABOUT SWAP-EZ</h5>
                            </div>
                            <ul className="uk-list uk-list-large">
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Newsroom
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Leadership
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Job Opportunities
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Investors
                                    </span>
                                </li>
                                <li className="uk-text-small uk-text-bold uk-flex uk-flex-middle">
                                    <span>
                                        Contact Swap-ez
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <hr className="uk-divider-small"/>
                    <div className="uk-flex uk-flex-column uk-flex-column@s uk-flex-row@l uk-flex-between@l">
                        <div className="uk-text-small uk-text-white
                            uk-flex uk-flex-middle">
                            © Swap*EZ 2018. All Rights Reserved.
                        </div>
                        <div className="uk-text-small uk-text-white
                            uk-flex uk-flex-middle uk-flex-left@m uk-flex-center@l uk-visible@m
                            uk-margin-remove-top@l uk-margin-top@s">
                            <div>
                                <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-xsmall-right">Privacy Policy</a>
                            </div>
                            <div>
                                <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-xsmall-right">Term Of Us</a>
                            </div>
                            <div>
                                <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-xsmall-right">Sales And Refunds</a>
                            </div>
                            <div>
                                <a className="uk-link-reset uk-border-link uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left uk-margin-xsmall-right">Legal</a>
                            </div>
                            <div>
                                <a className="uk-link-reset uk-text-small uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-left">Sitemap</a>
                            </div>
                        </div>
                        <div className="uk-text-small uk-text-white
                            uk-flex uk-flex-middle uk-flex-left@m uk-flex-right@l
                            uk-margin-remove-top@l uk-margin-top@s">
                            <img data-src="http://www.iconarchive.com/download/i86154/custom-icon-design/round-world-flags/Canada.ico"
                                width="20" height="20" uk-img="true"/>
                            <span className="uk-margin-xsmall-left">Canada</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;