import React from "react";
import Validator from "./validator";

export class InputRange extends React.Component{
    constructor(props){
        super(props);
        this.instanceRef = null;
    }
    componentDidMount(){
        new JSR([this.instanceRef], {
            sliders: 1,
            values: [100],
            step: 1,
            limit: {
                min: 50
            },
            labels: {
                formatter: (value) => {
                    return value.toString() + ' %';
                }
            },
            grid: false
        });
    }
    render(){
        const min = this.props.min ? this.props.min : 0;
        const max = this.props.max ? this.props.max : 0;
        return (
            <div className="uk-inline uk-width">
                <input ref={instance => this.instanceRef = instance} id={this.props.id} type="range" min={min} 
                    max={max}/>
            </div>
        )
    }
}

export class InputText extends React.Component{
    constructor(props){
        super(props);
        this.handlerInput = this.handlerInput.bind(this);
        this.state = {
            value: '',
            error: 0,
            message: ''
        }
        this.validator = null;
        this.isValid = true;
        this.isTouch = false;
        this.name = '';
    }
    componentWillReceiveProps(nextProps){
        this.isTouch = false;
        this.state.error = 0;
        this.handlerInput(nextProps.value);
    }
    componentDidMount(){
        this.handlerInput(this.props.value);
    }
    componentWillUnmount(){
        this.validator = null;
        this.isTouch = false;
        this.name = '';
    }
    setValidator(validator, name){
        this.validator = validator;
        this.name = name;
    }
    handlerInput(value){
        if(!is.existy(this.props.validations)){
            this.setState({value: value}, () => {
                if(is.function(this.props.onInput))
                    this.props.onInput(value);
            });
            return;
        }
        if(value.length > 0)
            this.isTouch = true;
        if(!this.isTouch){
            this.setState({value: value}, () => {
                Validator.validate(this, value, true);
            });
            return;
        }
        Validator.validate(this, value);
    }
    handlerClassInput(){
        let classList = 'uk-input';
        switch(this.state.error){
            case 0:
                break;
            case 1:
                classList += ' uk-form-danger';
                break;
            case 2:
                classList += ' uk-form-success';
                break;
            default:
                return;
        }
        return classList;
    }
    render(){
        const type = this.props.type ? this.props.type : 'text';
        return (
            <div className="uk-width">
                <div className="uk-inline uk-width">
                    {
                        this.props.icon ?
                        <span className="uk-form-icon" uk-icon={"icon: "+this.props.icon}/>
                        : null
                    }
                    <input className={this.handlerClassInput()} type={type} placeholder={this.props.placeholder}
                        readOnly={this.props.readOnly}
                        onChange={(event) => this.handlerInput(event.target.value)}
                        ref={instance => this.inputRef = instance}
                        value={this.state.value}
                        autoComplete="off"
                        disabled={this.props.disabled}
                        id={this.props.id}/>
                </div>
                {
                    this.state.message ?
                        <div className="uk-text-meta uk-text-danger">{this.state.message}</div>
                    : null
                }
            </div>
        )
    }
}

export class InputNumber extends React.Component{
    constructor(props){
        super(props);
        this.handlerInput = this.handlerInput.bind(this);
    }
    handlerInput(event){
        let value = event.target.value;
        let valueNum = Number(event.target.value);
        if(is.not.number(valueNum)){
            if (value != null && value.length > 0) {
                value = value.substring(0, value.length - 1);
            }
        }
        event.target.value = value;
    }
    render(){
        return (
            <div className="uk-inline uk-width">
                {
                    this.props.icon ?
                    <span className="uk-form-icon" uk-icon={"icon: "+this.props.icon}/>
                    : null
                }
                <input className="uk-input" type="text"
                    placeholder={this.props.placeholder}
                    readOnly={this.props.readOnly}
                    onInput={this.handlerInput}
                    id={this.props.id}/>
            </div>
        )
    }
}

export class InputPassword extends React.Component{
    constructor(props){
        super(props);
        this.handlerInput = this.handlerInput.bind(this);
        this.handlerToggle = this.handlerToggle.bind(this);
        this.state = {
            isShow: false,
            value: '',
            error: 0,
            message: ''
        }
        this.inputRef = null;
        this.validator = null;
        this.isValid = true;
        this.isTouch = false;
        this.name = '';
    }
    componentWillReceiveProps(nextProps){
        
    }
    componentDidMount(){
        this.handlerInput(this.props.value);
    }
    componentWillUnmount(){
        this.inputRef = null;
        this.validator = null;
        this.isValid = true;
        this.isTouch = false;
        this.name = '';
    }
    setValidator(validator, name){
        this.validator = validator;
        this.name = name;
    }
    handlerInput(value){
        if(!is.existy(this.props.validations)){
            this.setState({value: value}, () => {
                if(is.function(this.props.onInput))
                    this.props.onInput(value);
            });
            return;
        }
        if(value.length > 0)
            this.isTouch = true;
        if(!this.isTouch){
            this.setState({value: value}, () => {
                Validator.validate(this, value, true);
            });
            return;
        }
        Validator.validate(this, value);
    }
    handlerToggle(){
        this.setState({isShow: !this.state.isShow}, () => {
            $(this.inputRef).focus();
        });
    }
    handlerClassInput(){
        let classList = 'uk-input';
        switch(this.state.error){
            case 0:
                break;
            case 1:
                classList += ' uk-form-danger';
                break;
            case 2:
                classList += ' uk-form-success';
                break;
            default:
                return;
        }
        return classList;
    }
    render(){
        return (
            <div className="uk-width">
                <div className="uk-inline uk-width">
                    {
                        this.props.icon ?
                        <span className="uk-form-icon" uk-icon={"icon: "+this.props.icon}/>
                        : null
                    }
                    <input className={this.handlerClassInput()} type={this.state.isShow ? 'text': 'password'}
                        placeholder={this.props.placeholder}
                        readOnly={this.props.readOnly}
                        onInput={event => this.handlerInput(event.target.value)}
                        ref={instance => this.inputRef = instance}
                        autoComplete="new-password"
                        id={this.props.id}/>
                    <a className="uk-form-icon uk-link-reset uk-form-icon-flip"
                        onClick={this.handlerToggle}>
                        {this.state.isShow ? <i className="fa fa-eye"/> : <i className="fa fa-eye-slash"/>}
                    </a>
                </div>
                {
                    this.state.message ?
                        <div className="uk-text-meta uk-text-danger">{this.state.message}</div>
                    : null
                }
            </div>
        )
    }
}

export class Checkbox extends React.Component{
    constructor(props){
        super(props);   
    }
    render(){
        return (
            <div className="pretty p-svg">
                <input type="checkbox"/>
                <div className="state">
                    <span className="svg" uk-icon="icon: check"></span>
                    <label>
                        <span className="uk-text-meta">{this.props.label}</span>
                    </label>
                </div>
            </div>
)
    }
}