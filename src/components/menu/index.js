import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../hoc/storage";

class Menu extends React.Component{
    constructor(props){
        super(props);
        this.navigateToLogin = this.navigateToLogin.bind(this);
        this.navigateToHome = this.navigateToHome.bind(this);
        this.navigateToAccount = this.navigateToAccount.bind(this);
        this.handlerLogout = this.handlerLogout.bind(this);
    }
    navigateToHome(){
        this.props.history.push('/');
    }
    navigateToLogin(){
        this.props.history.push('/login');
    }
    navigateToAccount(){
        this.props.history.push('/account');
    }
    handlerLogout(){
        localStorage.removeItem('email');
        setTimeout(() => {
            this.props.history.push(this.props.history.location.pathname);
            UIkit.notification({message: 'Logout Successfully !', status: 'primary', pos: 'top-left'});
        }, 500);
    }
    render(){
        return (
            <div className="uk-background-primary" uk-sticky="true">
                <div className="uk-container">
                    <div className="uk-flex uk-text-white uk-flex-between">
                        <div className="uk-hidden@m uk-cursor uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-left uk-padding-remove-top uk-padding-remove-bottom uk-link-reset">
                            <div className="uk-inline">
                                <i className="fa fa-navicon"/>
                                <div uk-dropdown="mode: click">
                                    <ul className="uk-list uk-list-large uk-list-divider">
                                        <li>
                                            <a href="#">Smart Phones</a>    
                                        </li>
                                        <li>Tablets</li>
                                        <li>Smart Watches</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <a className="uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-left uk-padding-remove-top uk-padding-remove-bottom"
                            onClick={this.navigateToHome}>
                            <div>
                                <img data-src="http://demo.swap-ez.com/assets/logo2.bfe8cc381ae464b6045494b74be163fd.png" width="30" height="30" uk-img="true"/>
                            </div>
                        </a>
                        <a className="uk-visible@l uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                            Your Purpose
                        </a>
                        <a className="uk-visible@m uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                            Smart Phones
                        </a>
                        <a className="uk-visible@m uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                            Tablets
                        </a>
                        <a className="uk-visible@m uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                            Smart Watches
                        </a>
                        <a className="uk-visible@l uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                            Support
                        </a>
                        {
                            !this.props.auth
                            ?
                            <a className="uk-visible@m uk-background-muted uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset uk-text-small">
                                Register
                            </a>
                            : null
                        }
                        <a className="uk-visible@m uk-height-small uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-link-reset">
                            <i className="fa fa-search"/>
                        </a>
                        <div className="uk-height-small uk-cursor uk-flex uk-flex-middle uk-padding-small uk-padding-remove-right uk-padding-remove-top uk-padding-remove-bottom uk-link-reset">
                            <div className="uk-inline">
                                <i className="fa fa-shopping-bag"/>
                                <span className="uk-badge uk-position-top-right uk-position-cart">1</span>
                            </div>
                            <div uk-dropdown="pos: top-right">
                                <div className="uk-flex uk-flex-column">
                                    {
                                        this.props.auth
                                        ? 
                                        <a className="uk-flex uk-flex-middle uk-height-small" href="#" onClick={this.navigateToAccount}>
                                            <i className="fa fa-user"/>
                                            <div className="uk-margin-xsmall-left">My Account</div>
                                        </a>
                                        : null
                                    }
                                    {
                                        !this.props.auth
                                        ? 
                                        <a className="uk-flex uk-flex-middle uk-height-small" href="#" onClick={this.navigateToLogin}>
                                            <i className="fa fa-sign-out"/>
                                            <div className="uk-margin-xsmall-left">Login</div>
                                        </a>
                                        : null
                                    }
                                    {
                                        this.props.auth
                                        ? 
                                        <a className="uk-flex uk-flex-middle uk-height-small" href="#" onClick={this.handlerLogout}>
                                            <i className="fa fa-sign-out"/>
                                            <div className="uk-margin-xsmall-left">Logout</div>
                                        </a>
                                        : null
                                    }
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(withStorage(Menu));