import React from "react";

const withStorage = (WrappedComponent) => {
    class HOC extends React.Component{
        constructor(props){
            super(props);
        }
        render(){
            const email = localStorage.getItem('email') ? localStorage.getItem('email') : null;
            const auth = email ? email : null;
            const props = {
                auth: auth,
                ...this.props
            }
            return <WrappedComponent {...props} ref={instance => this.wrappedComponentRef = instance}/>;
        }
    }
    return HOC;
}

export default withStorage;