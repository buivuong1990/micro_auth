import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import createHistory from "history/createBrowserHistory";
import { Provider } from "react-redux";
import configureStore from "./configureStore";

import ScrollToTop from "./hoc/scrollToTop";

import Home from "./containers/home";
import Login from "./containers/login";
import Account from "./containers/account";

import Menu from "./components/menu";
import Footer from "./components/footer";

const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);

import withStorage from "./hoc/storage";

class App extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <Provider store={store}>
                <Router>
                    <ScrollToTop>
                        <div>
                            <div className="uk-height-viewport" uk-height-viewport="expand: true">
                                <Menu/>
                                <Route exact path="/" component={withStorage(Home)}/>
                                <Route exact path="/login" component={withStorage(Login)}/>
                                <Route exact path="/account" component={withStorage(Account)}/>
                            </div>
                            <div>
                                <Footer/>
                            </div>
                        </div>
                    </ScrollToTop>
                </Router>
            </Provider>
        )
    }
}

export default App;