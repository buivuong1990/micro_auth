const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require('path');

module.exports = {
    mode: 'development',
    entry: path.join(__dirname, '..', 'src-mobile', 'index.js'),
    output: {
        path: path.join(__dirname, '..', 'server', 'dist-mobile'),
        filename: 'mobile.bundle.js',
        publicPath: '/'
    },
    module: {
      rules: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader"
            }
        },
        {
            test: /\.html$/,
            use: [
                {
                    loader: "html-loader",
                    options: { minimize: true }
                }
            ]
        }
      ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src-mobile/index.html",
            filename: "./mobile.html"
        })
    ]
};